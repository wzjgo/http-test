package main

import (
	"fmt"
	"log"
	"net/http"
)

func indexHandler(w http.ResponseWriter, r *http.Request) {
	log.Printf("res %#v\n", w)
	fmt.Fprintf(w, "hello world")
}

func main() {
	fmt.Println("start")
	http.HandleFunc("/", indexHandler)
	http.ListenAndServe(":8001", nil)
}
