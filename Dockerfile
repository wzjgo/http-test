FROM golang:1.16 AS builder
# RUN apk add --no-cache ca-certificates git

WORKDIR /mcloud-paas
COPY . .
RUN go build -o server .

FROM alpine AS release
# RUN apk add --no-cache ca-certificates
WORKDIR /mcloud-paas
COPY --from=builder /mcloud-paas/server ./server
ENTRYPOINT ["/mcloud-paas/server"]
